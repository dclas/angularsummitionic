// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('master', {
    url: '/',
    templateUrl: 'templates/master.html',
    controller: 'BeeCtrl as master'
  })
  .state('newItem', {
    url: '/new',
    templateUrl: 'templates/newitem.html',
    controller: 'NewItemController as newitem'
  })
  .state('itemDetail', {
    url: '/detail/:itemId',
    templateUrl: 'templates/itemdetail.html',
    controller: 'BeeDetailCtrl as bee'
  })
  $urlRouterProvider.otherwise('/');
})
.service('BeeService', function() {
  var items = [
    {id:1, name: 'Honey Bee'},
    {id:2, name: 'Killer Bee'},
    {id:3, name: 'Hybrid Asshole Bee'},
  ];
  return {
    all: function() {
      return items;
    },
    newItem: function(beeToAdd) {
      items.unshift({
        name: beeToAdd,
        id: items.length+1
      })
    },
    remove: function(beeToRemove) {
      items.splice(items.indexOf(beeToRemove), 1);
    },
    getBee: function(beeId) {
      console.log(beeId);
      for(var i=0; i<items.length; i++) {
        if(items[i].id === parseInt(beeId.itemId)) {
          return items[i];
        }
      }

    }
  }
})
.controller('BeeCtrl', function($scope, BeeService) {
  var master = this;
  master.items = BeeService.all();
  this.removeItem = function(beeToRemove) {
    BeeService.remove(beeToRemove);
  }
})
.controller('NewItemController', function(BeeService, $ionicHistory, $ionicPlatform, $cordovaCamera) {

  var newitem = this;

  this.addNewItem = function(beeToAdd) {
    BeeService.newItem(beeToAdd);
    $ionicHistory.goBack();
  }

  newitem.img = '';

  $ionicPlatform.ready(function() {

    var options = {
      quality: 100,
      allowEdit: true,
      targetWidth: 200,
      targetHeight: 200,
      saveToPhotoAlbum: true,
      correctOrientation:true
    };

    newitem.takePicture = function() {
      console.log("Click");
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var image = document.getElementById('myImage');
        this.img = "data:image/jpeg;base64," + imageData;
      }, function(err) {
        console.log(err);
        alert("You ain't gots no camera, fool!");
      });
    }

  });

})
.controller('BeeDetailCtrl', function(BeeService, $stateParams) {
  var bee = this;
  bee.currentItem = BeeService.getBee($stateParams);
})
